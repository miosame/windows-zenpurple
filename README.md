# windows-zenpurple

Made possible thanks to the original zenpurple theme:
- [link to original repo](https://gitlab.com/syne/freebsd)
- [webarchived screenshot](https://web.archive.org/web/20190420210854/https://i.imgur.com/P2iBvcX.png)
- [webarchived readme](https://web.archive.org/web/20190420210809/https://gitlab.com/syne/freebsd/commit/ecb09cd93b75ef3a787ff5d69b63244997fae717)

exported into terminal configs with http://terminal.sexy

![Screenshot](https://p.miosa.me/miosame/windows-zenpurple/raw/branch/master/screenshot.png)

# config

mintty/git-bash config location: `AppData\Roaming\mintty\config`